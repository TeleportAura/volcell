use super::*;

#[test]
fn read_write() {
    let mut volatile_u8 = VolatileCell::new(4u8);
    volatile_u8.write(8);
    assert_eq!(volatile_u8.read(), 8);
}

/// This test shows how VolatileCells can also
/// be used in packed structs.  
/// For this to work on architectures besides x86, the unstable
/// flag needs to be enabled
#[cfg(any(feature = "unstable", target_arch = "x86_64"))]
#[test]
fn read_write_packed() {
    #[repr(C, packed)]
    struct PackedType {
        _unused_1: u8,
        _unused_2: bool,
        volatile: VolatileCell<u8, ReadWriteAccess, PtrUnaligned>,
        _unused_3: u128
    }
    let mut packed_struct = PackedType {
        _unused_1: 4,
        _unused_2:  false,
        volatile: VolatileCell::new_with(3, ReadWriteAccess, PtrUnaligned),
        _unused_3: 1337,
    };
    packed_struct.volatile.write({
        packed_struct.volatile.read() + 4
    });
    assert_eq!(packed_struct.volatile.read(), 7);
}

#[test]
fn read_write_custom_alignment() {
    type AlignStart = u8;
    type AlignEnd = u32;

    #[repr(C, packed)]
    struct CustomAlignment {
        volatile: VolatileCell<u16, ReadWriteAccess, PtrUnaligned, AlignStart, AlignEnd>,
    }

    let mut custom_align = CustomAlignment {
        volatile: VolatileCell::new_with(3, ReadWriteAccess, PtrUnaligned),
    };
    assert_eq!(core::mem::size_of::<CustomAlignment>(), 7);
    custom_align.volatile.write(1337);
    assert_eq!(custom_align.volatile.read(), 1337);
}
