//! The volcell crates provides the [`VolatileCell`] type which
//! is a container that enforces volatile access (i.e. memory access
//! that cannot be optimized away by the compiler) for its contents.
//! This is very useful when doing systems programming, where one might
//! have to communicate with other programs, the OS or even the CPU itself
//! using memory-mapped I/O.
//! At its most basic, volcell is very easy to use:
//! ```
//! extern crate volcell;
//! use volcell::*; 
//!
//! let mut volatile_int = VolatileCell::<i32>::new(4);
//! volatile_int.write(1337);
//! assert_eq!(1337, volatile_int.read());
//! ```
//! It is, however, also possible to use generic parameters to change almost any
//! aspect of the way VolatileCell works:
//! ```
//! extern crate volcell;
//! use volcell::*;
//!
//! let mut volatile_int = VolatileCell::<
//!     i32, // the cell contains an i32, like above
//!     ReadWriteAccess, // it can be read from and written to
//!     PtrUnaligned, // it performs unaligned memory access
//!     u16, // it also contains an empty u16 before the i32 (e.g. for alignment reasons)
//!     u8 // and one empty byte after the u8
//! >::new_with(4, ReadWriteAccess, PtrUnaligned);
//! volatile_int.write(1337);
//! assert_eq!(1337, volatile_int.read());
//! ```


#![no_std]
#![cfg_attr(feature = "unstable", feature(core_intrinsics))]
#![allow(unused)]

use core::marker::PhantomData;
use core::ptr;
#[cfg(feature = "unstable")]
use core::intrinsics;

#[cfg(test)]
mod tests;

pub trait Access {}
pub trait WrAccess: Access {}
pub trait RdAccess: Access {}

pub unsafe trait Alignment {
    unsafe fn write<T>(ptr: *mut T, data: T);
    unsafe fn read<T>(ptr: *const T) -> T;
}
pub trait Unaligned: Alignment {}
pub trait Aligned: Alignment {}

#[derive(Debug, Default)]
pub struct PtrAligned;
unsafe impl Alignment for PtrAligned {
    #[inline(always)]
    unsafe fn write<T>(ptr: *mut T, data: T) {
        ptr::write_volatile(ptr, data);
    }
    #[inline(always)]
    unsafe fn read<T>(ptr: *const T) -> T {
        ptr::read_volatile(ptr)
    }
}
impl Aligned for PtrAligned {}

#[cfg(any(
    feature = "unstable",
    target_arch  = "x86",
    target_arch = "x86_64"
))]
#[derive(Debug, Default)]
pub struct PtrUnaligned;
#[cfg(all(
    feature = "unstable",
    not(
        any(
            target_arch = "x86",
            target_arch = "x86_64"
        )
    )
))]
unsafe impl Alignment for PtrUnaligned {
    #[inline(always)]
    unsafe fn write<T>(ptr: *mut T, data: T) {
        intrinsics::unaligned_volatile_store(ptr, data);
    }
    #[inline(always)]
    unsafe fn read<T>(ptr: *const T) -> T {
        intrinsics::unaligned_volatile_load(ptr)
    }
}
#[cfg(any(target_arch = "x86", target_arch = "x86_64"))]
unsafe impl Alignment for PtrUnaligned {
    #[inline(always)]
    unsafe fn write<T>(ptr: *mut T, data: T) {
        ptr::write_volatile(ptr, data)
    }
    #[inline(always)]
    unsafe fn read<T>(ptr: *const T) -> T {
        ptr::read_volatile(ptr)
    }
}

#[cfg(any(
    feature = "unstable",
    target_arch  = "x86",
    target_arch = "x86_64"
))]
impl Unaligned for PtrUnaligned {}

#[derive(Debug, Default)]
pub struct ReadWriteAccess;
impl Access for ReadWriteAccess {}
impl WrAccess for ReadWriteAccess {}
impl RdAccess for ReadWriteAccess {}

#[derive(Debug, Default)]
pub struct ReadOnlyAccess;
impl Access for ReadOnlyAccess {}
impl RdAccess for ReadOnlyAccess {}

#[allow(dead_code)]
#[derive(Debug, Default)]
#[repr(C, packed)]
pub struct VolatileCell<T, A: Access = ReadWriteAccess, P: Alignment = PtrAligned, S = (), E = ()> {
    start: S,
    inner: T,
    _access: PhantomData<A>,
    _alignment: PhantomData<P>,
    end: E,
}

impl<T> VolatileCell<T> {
    pub fn new(inner: T) -> Self {
        Self {
            start: (),
            inner,
            _access: PhantomData::<ReadWriteAccess>,
            _alignment: PhantomData::<PtrAligned>,
            end: (),
        }
    }

    pub fn new_readonly(inner: T) -> VolatileCell<T, ReadOnlyAccess> {
        VolatileCell {
            start: (),
            inner,
            _access: PhantomData::<ReadOnlyAccess>,
            _alignment: PhantomData::<PtrAligned>,
            end: (),
        }
    }

}

impl<T, A: Access, P: Alignment, S: Default, E: Default> VolatileCell<T, A, P, S, E> {
    pub fn new_with(inner: T, access: A, pointer_alignment: P) -> Self {
        Self {
            start: Default::default(),
            inner,
            _access: PhantomData::<A>,
            _alignment: PhantomData::<P>,
            end: Default::default(),
        }
    }
}

impl<T, A: Access, P: Unaligned, S, E> VolatileCell<T, A, P, S, E> {
    pub fn new_with_alignment(inner: T, access: A, pointer_alignment: P, start: S, end: E) -> Self {
        Self {
            start, 
            inner,
            _access: PhantomData::<A>,
            _alignment: PhantomData::<P>,
            end,
        }
    }
}

impl<T, A: RdAccess, P: Alignment, S, E> VolatileCell<T, A, P, S, E> {
    #[inline(always)]
    pub fn read(&self) -> T {
        unsafe {
            P::read(self as *const _ as *const _)
        }
    }
}

impl<T, A: WrAccess, P: Alignment, S, E> VolatileCell<T, A, P, S, E> {
    #[inline(always)]
    pub fn write(&mut self, value: T) {
        unsafe {
            P::write(self as *mut _ as *mut _, value);
        }
    }
}
