# volcell

The volcell crate provides the container type `VolatileCell` which prevents memory accesses to its data
from being optimized away by the compiler.  
This is useful for example when writing to memory that needs to be read by either another process or the kernel
and when writing to buffers that are used to interface with hardware (e.g. the VGA buffer or an APIC register).  
`VolatileCell` can be also used in packed structs, however, for this to be possible on processor architectures other than x86 you need to enable the `unstable` feature and use the nightly Rust toolchain.

## Example usage

Take a look at the tests found in [this file](src/tests.rs)

## Contributing

This project is open to any sorts of contributions, be it by notifying me of an issue with the library
or by directly contributing code. Since I want to keep this library small and simple, however, you should
open an issue to discuss the changes you want to make before starting work on a pull request.

## License

Copyright 2023, TeleportAura
This software is licensed under the [MPL 2.0](LICENSE) license
